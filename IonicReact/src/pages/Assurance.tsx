import {
  IonTitle,
  IonLabel,
  IonItem,
  IonButton,
  IonGrid,
  IonRow,
  IonCol,
  IonPage,
  IonContent,
} from "@ionic/react";
import { useEffect, useState } from "react";

const Assurance : React.FC = () => {
  const url1mois = "http://localhost:8080/assurance/expire/1";
  const url3mois = "http://localhost:8080/assurance/expire/3";

  const [listeassurance, setListeassurance] = useState<any>([]);

  const get1Mois = () => {
    fetch(url1mois, {
      method: "get",
      headers: { "Content-type": "application/json" },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setListeassurance(data);
      });
  };
  useEffect(() => {
    get1Mois();
  }, []);

  const get3Mois = () => {
    fetch(url3mois, {
      method: "get",
      headers: { "Content-type": "application/json" },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setListeassurance(data);
      });
  };
  useEffect(() => {
    get3Mois();
  }, []);

  return (
    <>
    <IonPage>
      <IonContent>
      <IonTitle>Assurance</IonTitle>
      <IonItem>
        <IonLabel>EXPIRATION DANS </IonLabel>
        <IonButton type="submit" onClick={() => get1Mois()}>
          1 mois
        </IonButton>
        <IonButton type="submit" onClick={() => get3Mois()}>
          3 mois
        </IonButton>
      </IonItem>

      <IonGrid>
        <IonRow>
          <IonCol>
            {" "}
            <strong>Avion</strong>{" "}
          </IonCol>
          <IonCol>
            {" "}
            <strong>date debut</strong>{" "}
          </IonCol>
          <IonCol>
            {" "}
            <strong>date fin</strong>{" "}
          </IonCol>
        </IonRow>
        {listeassurance.map((det: any) => {
          return (
            <IonRow>
              <IonCol>{det.marque + " " + det.immatricule}</IonCol>
              <IonCol>{det.datedebut}</IonCol>
              <IonCol>{det.datefin}</IonCol>
            </IonRow>
          );
        })}
      </IonGrid>
      </IonContent>
      </IonPage>
    </>
  );
};

export default Assurance;
