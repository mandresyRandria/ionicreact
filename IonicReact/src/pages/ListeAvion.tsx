import React, { useState, useEffect } from "react";
import {IonCol, IonGrid, IonRow, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './detailVehicule.css';

const ListeAvion: React.FC = () => {
  const avionurl = "http://localhost:8080/avions";
  const [avion, setIdavion] = useState<any>([]);
  const getIdavion = () => {
    fetch(avionurl, {
      method: "get",
      headers: { "Content-type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log(data)
        setIdavion(data);
      });
  };

  const urldetail = "http://localhost:8080/avion/";
  const [detail, setDetail] = useState<any>([]);

  const [idavion, setEtat] = useState("");
  const getdetail = () => {
    fetch( urldetail + idavion + "/kilometrages", {
      method: "get",
      headers: { "Content-type": "application/json" },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDetail(data);
      });
  };
  useEffect(() => {
    getdetail();
  }, []);
  useEffect(() => {
    getIdavion();
  }, []);

  return (
    <>
    {<IonPage>
        <IonHeader>
            <IonToolbar>
              <IonTitle>Liste des avions</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
            <IonGrid>
                <IonRow>
                    <IonCol> <strong>Marque Avion</strong> </IonCol>
                    <IonCol> <strong>Immatricule Avion</strong> </IonCol>
                    <IonCol> <strong></strong> </IonCol>
                </IonRow>
                {avion.map((davion: any) => {
                    return (
                        <IonRow>
                            <IonCol>{davion.marque}</IonCol>
                            <IonCol>{davion.immatricule}</IonCol>
                            <IonCol>
                                <button onClick={()=> {setEtat(davion.id); getdetail()} }>Détails</button>
                            </IonCol>
                        </IonRow>
                    );
                })}
            </IonGrid>
        </IonContent>

        
            <IonToolbar>
              <IonTitle>Détails de l'avion</IonTitle>
               
                
                
          </IonToolbar>
            <IonGrid>
                        <IonRow>
                            <IonCol> <strong>Avion</strong> </IonCol>
                            <IonCol> <strong>Date</strong> </IonCol>
                            <IonCol> <strong>Début Km</strong> </IonCol>
                            <IonCol> <strong>Fin Km</strong> </IonCol>
                        </IonRow>
            {detail.map((det: any) => {
                      
                        return (
                            <IonRow>
                                <IonCol>{det.marque} {det.immatricule}</IonCol>
                                <IonCol>{det.datecourse}</IonCol>
                                <IonCol>{det.debutkm}</IonCol>
                                <IonCol>{det.finkm}</IonCol>
                            </IonRow>
                        );
                      }
                  )}
            </IonGrid>
       

    </IonPage>}

              
    </>
  );
};

export default ListeAvion;
