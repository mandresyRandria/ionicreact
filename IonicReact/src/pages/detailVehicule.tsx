import {IonCol, IonGrid, IonRow, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './detailVehicule.css';


const DetailVehicule: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Détail véhicule</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonGrid>
            <IonRow>
                <IonCol> <strong>Marque Vehicule</strong> </IonCol>
                <IonCol> <strong>Matricule Vehicule</strong> </IonCol>
                <IonCol> <strong>Date cource</strong> </IonCol>
                <IonCol> <strong>Debut kilomatrage</strong></IonCol>
                <IonCol> <strong>Fin kilomatrage</strong></IonCol>
            </IonRow>
            
            <IonRow>
                <IonCol>Toyota</IonCol>
                <IonCol>blablabla</IonCol>
                <IonCol>10 Mars 2020</IonCol>
                <IonCol>10 km</IonCol>
                <IonCol>18 km</IonCol>
            </IonRow>
            <IonRow>
                <IonCol>Toyota</IonCol>
                <IonCol>blablabla</IonCol>
                <IonCol>10 Mars 2020</IonCol>
                <IonCol>10 km</IonCol>
                <IonCol>18 km</IonCol>
            </IonRow>
            <IonRow>
                <IonCol>Toyota</IonCol>
                <IonCol>blablabla</IonCol>
                <IonCol>10 Mars 2020</IonCol>
                <IonCol>10 km</IonCol>
                <IonCol>18 km</IonCol>
            </IonRow>
            <IonRow>
                <IonCol>Toyota</IonCol>
                <IonCol>blablabla</IonCol>
                <IonCol>10 Mars 2020</IonCol>
                <IonCol>10 km</IonCol>
                <IonCol>18 km</IonCol>
            </IonRow>
            <IonRow>
                <IonCol>Toyota</IonCol>
                <IonCol>blablabla</IonCol>
                <IonCol>10 Mars 2020</IonCol>
                <IonCol>10 km</IonCol>
                <IonCol>18 km</IonCol>
            </IonRow>
        </IonGrid>


      </IonContent>
    </IonPage>
  );
};

export default DetailVehicule;
