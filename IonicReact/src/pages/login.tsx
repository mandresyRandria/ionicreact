import {
  IonButton,
  IonInput,
  IonList,
  IonItem,
  IonLabel,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useEffect, useState } from "react";
import "./login.css";




const Login: React.FC = () => {

const [identifiant, setIdentifiant] = useState("admin@admin.com");
const [password, setPassword] = useState("admin");


const loginurl = "http://localhost:8080/administrateur/login";
const [log, setLog] = useState<any>([]);



const getLogin = () => {
  fetch(loginurl, {
    method: "post",
    headers: { "Content-type": "application/json" },
    body : JSON.stringify({email : identifiant, motdepasse: password})
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);
      setLog(data);
    });
};

useEffect(() => {
  getLogin();
}, []);





  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Page de connexion</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
          <IonList>
            <IonItem>
              <IonLabel position="floating">Identifiant</IonLabel>
              <IonInput
                id="identifiant"
                required
                name="identifiant"
                placeholder="Entrer votre identifiant"
                value={identifiant}
              ></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Mot de passe</IonLabel>
              <IonInput
                required
                name="motdepasse"
                type="password"
                value={password}
                placeholder="Entrer votre mot de passe"
              ></IonInput>
            </IonItem>
            <IonButton onClick={() => getLogin()} expand="block">
              Se Connecter
            </IonButton>
            
          </IonList>
                  
      </IonContent>
    </IonPage>
  );
};

export default Login;
